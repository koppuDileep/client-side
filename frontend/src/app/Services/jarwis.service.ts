import { HttpClient } from '@angular/common/http';
import { ConditionalExpr } from '@angular/compiler';
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class JarwisService {

  private baseUrl = 'http://localhost:9000/bug_tracking_system/api/v1/login';
  constructor(private http:HttpClient) { }

  Signup (data) {
    return   this.http.post('${this.baseUrl}/signup', data) 
  }

  login(data) {
    console.log(data);
    
    
    return   this.http.post('http://localhost:9000/bug_tracking_system/api/v1/login', data) 
  }

  sendPasswordResetLink(data){
    return this.http.post('${this.baseUrl}/sendPasswordResetLink', data)
  }
   
  changePassword(data){
    return this.http.post('${this.baseUrl}/resetPassword', data)
  }

}
