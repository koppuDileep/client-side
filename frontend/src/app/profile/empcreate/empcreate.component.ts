import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'; 
import { IssueService } from '../../issue.service';


@Component({
  selector: 'app-empcreate',
  templateUrl: './empcreate.component.html',
  styleUrls: ['./empcreate.component.css']
})
export class EmpcreateComponent implements OnInit {

  createForm: FormGroup;

  constructor(private issueService: IssueService, private fb: FormBuilder, private router: Router) {
    this.createForm = this.fb.group({
      username: ['', Validators.required],
      email: '',
      role_ids: '',
      firstname:' ',
      lastname:'',
      mobile:'',
      gender:'',
      dob:'',
      password:'',
      conformpassword:'',
      parentname:'',
      parent_contact_no:'',
      address:'',
      state:'',
      city:'',
      pincode:'',
      is_active:'',
     bug_image:'',
   
      
      
    });
  }

  addDetails(username, email , role_ids,firstname,lastname,mobile,gender,dob,password,conformpassword,parentname,parent_contact_no,address,state,city,pincode,is_active,bug_image,status) {
    this.issueService.addDetails(username, email, role_ids,firstname,lastname,mobile,gender,dob,password,conformpassword,parentname,parent_contact_no,address,state,city,pincode,is_active,bug_image,status).subscribe(() => {
      this.router.navigate(['/emplist']);
    });
  }

  ngOnInit() {
  }

}
