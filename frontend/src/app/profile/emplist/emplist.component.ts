import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

import { Issue } from '../../issue.model';
import { IssueService } from '../../issue.service';

@Component({
  selector: 'app-emplist',
  templateUrl: './emplist.component.html',
  styleUrls: ['./emplist.component.css']
})
export class EmplistComponent implements OnInit {

  issues: Issue[];
  displayedColumns = ['username', 'email', 'role_ids','firstname','lastname','mobile','gender','dob','password','conformpassword','parentname','parent_contact_no','address','state','city','pincode','is_active','bug_image',  'status', 'actions'];

  constructor(private issueService: IssueService, private router: Router) { }

  ngOnInit() {
    this.fetchDetails();
  }

  fetchDetails() {
    this.issueService
      .getDetails()
      .subscribe((data: Issue[]) => {
        this.issues = data;
        console.log('Data requested ...');
        console.log(this.issues);
      });
  }

  editDetails(id) {
    this.router.navigate([`/empedit/${id}`]);
  }

  deleteDetails(id) {
    this.issueService.deleteDetails(id).subscribe(() => {
      this.fetchDetails();
    });
  }

}
