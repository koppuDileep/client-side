import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material';

import { IssueService } from '../../issue.service';


@Component({
  selector: 'app-empedit',
  templateUrl: './empedit.component.html',
  styleUrls: ['./empedit.component.css']
})
export class EmpeditComponent implements OnInit {

  id: String;
  issue: any = {};
  updateForm: FormGroup;

  // tslint:disable-next-line:max-line-length
  constructor(private issueService: IssueService, private router: Router, private route: ActivatedRoute, private snackBar: MatSnackBar, private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.updateForm = this.fb.group({
      username: ['', Validators.required],
      email: '',
      role_ids: '',
      firstname:' ',
      lastname:'',
      mobile:'',
      gender:'',
      dob:'',
      password:'',
      conformpassword:'',
      parentname:'',
      parent_contact_no:'',
      address:'',
      state:'',
      city:'',
      pincode:'',
      is_active:'',
      status: ''
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.issueService.getDetailsById(this.id).subscribe(res => {
        this.issue = res;
        this.updateForm.get('username').setValue(this.issue.title);
        this.updateForm.get('email').setValue(this.issue.responsible);
        this.updateForm.get('role_ids').setValue(this.issue.description);
       
        this.updateForm.get('status').setValue(this.issue.status);
      });
    });
  }

  updateDetails(username, email, role_ids,firstname,lastname,mobile,gender,dob,password,conformpassword,parentname,parent_contact_no,address,state,city,pincode,is_active,bug_image,  status) {
    this.issueService.updateDetails(this.id, username, email, role_ids,firstname,lastname,mobile,gender,dob,password,conformpassword,parentname,parent_contact_no,address,state,city,pincode,is_active,bug_image , status).subscribe(() => {
      this.snackBar.open('Details updated successfully', 'OK', {
        duration: 3000
      });
    });
  }

}
