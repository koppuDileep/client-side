import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class IssueService {

  uri = 'http://localhost:4000';

  constructor(private http: HttpClient) { }

  getIssues() {
    return this.http.get(`${this.uri}/issues`);
  }

  getDetails() {
    return this.http.get(`${this.uri}/issues`);
  }

  getIssueById(id) {
    return this.http.get(`${this.uri}/issues/${id}`);
  }

  getDetailsById(id) {
    return this.http.get(`${this.uri}/issues/${id}`);
  }

  addIssue(title,responsible, description,severity,status ) {
    const issue = {
      title: title,
      description: description,
      responsible: responsible,
      severity: severity,
      status: status
     
    };
    return this.http.post(`${this.uri}/issues/add`, issue);
  }

  addDetails(username,email, role_ids,firstname,lastname,mobile,gender,dob,password,conformpassword,parentname,parent_contact_no,address,state,city,pincode,is_active,bug_image,status ) {
    const issue = {
      title: username,
      description: email,
      responsible: role_ids,
      firstname:firstname,
      lastname:lastname,
      mobile:mobile,
      gender:gender,
      dob:dob,
      password:password,
      conformpassword:conformpassword,
      parentname:parentname,
      parent_contact_no:parent_contact_no,
      address:address,
      state:state,
      city:city,
      pincode:pincode,
      is_active:is_active,
      bug_image,
      status: status
     
    };
    return this.http.post(`${this.uri}/issues/add`,issue);
  }

  updateIssue(id, title,responsible, description, severity,  status) {
    const issue = {
      title: title,
      description: description,
      responsible: responsible,
      severity:severity,
      status: status
    };
    return this.http.post(`${this.uri}/issues/update/${id}`, issue);
  }


  updateDetails(id, username,email, role_ids,firstname,lastname,mobile,gender,dob,password,conformpassword,parentname,parent_contact_no,address,state,city,pincode,is_active,bug_image,   status) {
    const issue = {
      username: username,
      email: email,
      role_ids: role_ids,
      firstname:firstname,
      lastname:lastname,
      mobile:mobile,
      gender:gender,
      dob:dob,
      password:password,
      conformpassword:conformpassword,
      parentname:parentname,
      parent_contact_no:parent_contact_no,
      address:address,
      state:state,
      city:city,
      pincode:pincode,
      is_active:is_active,
      bug_image,
      status: status
    };
    return this.http.post(`${this.uri}/issues/update/${id}`, issue);
  }

  deleteIssue(id) {
    return this.http.get(`${this.uri}/issues/delete/${id}`);
  }

  deleteDetails(id) {
    return this.http.get(`${this.uri}/issues/delete/${id}`);
  }
}


