export interface Issue {
    id: String;
    title: String;
    responsible: String;
    description: String;
    severity: String;
    status: String;
    username: String;
    email: String;
    role_ids: String;
    firstname:string;
    lastname:string;
    mobile:string;
    gender:string;
    dob:string;
    password:string;
    conformpassword:string;
    parentname:string;
    parent_contact_no:string;
    address:string;
    state:string;
    city:string;
    pincode:string;
    is_active:string;

}
